#include <stdio.h>
#include <stdlib.h>

#include <vector>

#include <GL/glew.h>
#include <GL/glfw.h>

// Cg headers
#include <Cg/cg.h>
#include <Cg/cgGL.h>

// GLM headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr
#include <glm/gtx/vector_angle.hpp>

#include "objloader.hpp"
#include "GLGeometry.hpp"
#include "Player.hpp"

using namespace glm;

const int winHeight = 768, winWidth = 1024;

CGcontext		cgContext;
CGeffect 		cgEffect;
CGtechnique		cgTechnique;

CGparameter		shaderWVP, shaderWorld,shaderWorldIT, shaderViewInv, shaderTexture,  playerPosition, flashlightDirection, flashlightIntensity;

mat4 mProjection = perspective(120.0f,(float)winWidth / winHeight, 0.1f, 100.f);
mat4 mView = translate( mat4(1.0f), vec3(0.0f, 0.0f, -1.0f));
mat4 mModel = mat4(1.0f);
mat4 mWorld;
mat4 wvp;


const char winTitle[] = "Game Dev EX 2";

void initGL(const char * title) {
	if( !glfwInit() ) {
		fprintf( stderr, "Failed to initialise GLFW\n" );
		exit(EXIT_FAILURE);
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT,GL_FALSE);

	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( winWidth, winHeight, 0,0,0,0, 32,0, GLFW_FULLSCREEN ) ) {
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Initialise GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialise GLEW\n");
		exit(EXIT_FAILURE);
	}

	glfwSetWindowTitle( title);

	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );
	glfwDisable(GLFW_MOUSE_CURSOR);
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
}

void initCg (const char * fx_file_path) {

	if(!(cgContext = cgCreateContext())) {
		fprintf(stderr, "Failed to create Cg Context\n");
		exit(EXIT_FAILURE);
	}

	cgGLRegisterStates(cgContext);

	// compile the effect from file
	if(!(cgEffect = cgCreateEffectFromFile(cgContext, fx_file_path, NULL))) {
		fprintf(stderr, "Failed to load effect file: %s\n\n%s\n", fx_file_path,  cgGetLastListing(cgContext));
		exit(EXIT_FAILURE);
	}

	printf("Available techniques:\n\n");
	int i=0;
	for(cgTechnique = cgGetFirstTechnique(cgEffect); cgTechnique; cgTechnique = cgGetNextTechnique(cgTechnique)) {
		printf("%d: '%s'   :    %s.\n", i++,
				cgGetTechniqueName(cgTechnique),
				cgValidateTechnique(cgTechnique) == CG_TRUE?"VALID":"INVALID");
	}

	for(cgTechnique = cgGetFirstTechnique(cgEffect);
			cgTechnique && cgValidateTechnique(cgTechnique) == CG_FALSE;
			cgTechnique = cgGetNextTechnique(cgTechnique));

	if (cgTechnique) {
		printf("Using technique %s.\n", cgGetTechniqueName(cgTechnique));
	} else {
		fprintf(stderr, "No valid technique found in %s\n", fx_file_path);
		exit(EXIT_FAILURE);
	}

	// Get handles to parameters in the shader so that we can access them from CPU-side code
	if( !(shaderWVP = cgGetNamedEffectParameter(cgEffect, "gWvpXf")) ||
		!(shaderWorld = cgGetNamedEffectParameter(cgEffect, "gWorldXf")) ||
		!(shaderWorldIT = cgGetNamedEffectParameter(cgEffect, "gWorldITXf")) ||
		!(shaderViewInv = cgGetNamedEffectParameter(cgEffect, "gViewIXf")) ||
		!(shaderTexture = cgGetNamedEffectParameter(cgEffect, "gSampler")) ||
		!(playerPosition = cgGetNamedEffectParameter(cgEffect, "gPlayerPosition")) ||
		!(flashlightDirection = cgGetNamedEffectParameter(cgEffect, "gFlashlightDirection")) ||
		!(flashlightIntensity = cgGetNamedEffectParameter(cgEffect, "gFlashlightIntensity"))
	)
	{
		fprintf(stderr, "Cannot get a handle to a shader parameter\n");
		exit(EXIT_FAILURE);
	}
}

GLuint loadTexture(const char* imagepath) {

	GLuint tex;  //variable for texture

	glGenTextures(1,&tex); //allocate the memory for texture
	glBindTexture(GL_TEXTURE_2D,tex); //Binding the texture

	if(!glfwLoadTexture2D(imagepath, GLFW_BUILD_MIPMAPS_BIT)){
		fprintf(stderr, "Error opening %s\n", imagepath);
		exit(EXIT_FAILURE);
	}

	return tex;
}

void updateFPSCounter(float dt){
	static char fps[255];
	static float nFrames = 0;
	static float t = 0.0f;
	nFrames++;
	t += dt;
	if( t >= 1.0) {
		sprintf(fps, "%s: %f fps", winTitle, nFrames/t);
		glfwSetWindowTitle(fps);
		nFrames = 0.0f;
		t = 0;
	}
}

void updateMatrixParameters(){
	wvp = mProjection * mView * mWorld;

	cgGLSetMatrixParameterfc(shaderWVP, value_ptr(wvp));
	cgGLSetMatrixParameterfc(shaderWorld, value_ptr(mWorld));
	cgGLSetMatrixParameterfc(shaderWorldIT, value_ptr(transpose(inverse(mWorld))));
	cgGLSetMatrixParameterfc(shaderViewInv, value_ptr(inverse(mView)));
}

void updateWorld(float t, float dt, Player * player) {
	float flashlightAngle = angle(player->getFlashlightDirection(), vec3(0,0,-1));
	vec3 axis = cross(player->getFlashlightDirection(), vec3(0,0,-1));

	mView = translate( mat4(1.0f), vec3(0.0f, 0.0f, -1.0f));
	mView = rotate(mView, flashlightAngle/10, axis );
	wvp = mProjection * mView * mWorld;

	// pass matrix parameters to shader
	updateMatrixParameters();

	cgGLSetParameter3fv(flashlightDirection, value_ptr(player->getFlashlightDirection()));
	cgGLSetParameter1d(playerPosition, player->getPosition());
	cgGLSetParameter1d(flashlightIntensity, player->getFlashlight()->getCurrentIntensity(t));
}

void Cleanup() {
	glfwTerminate();
	cgDestroyContext(cgContext);
}

void handleInput(double dt, Player * player){
	if(glfwGetKey( 'W' ) == GLFW_PRESS)
		player->goForward(dt);

	if(glfwGetKey( 'S' ) == GLFW_PRESS)
		player->goBackward(dt);

	int x,y;
	glfwGetMousePos(&x, &y);

	vec4 viewPort(0,0,winWidth, winHeight);

	vec3 viewPos = unProject(vec3(x,y,1), mView*mModel, mProjection, viewPort);
	viewPos.y *= -1;
	viewPos = normalize(viewPos);
	player->pointFlashlightAt(viewPos);
}

int main( int argc, char** argv ) {
	initGL(winTitle);
	initCg("cave.cgfx");

	GLGeometry floor("plane.ob");
	floor.setWorldMatrix(translate(0.0f, -1.0f, 0.0f));

	GLGeometry ceiling("plane.ob");
	ceiling.setWorldMatrix(translate(0.0f, 1.0f, 0.0f));

	GLGeometry leftWall("plane.ob");
	leftWall.setWorldMatrix(rotate(translate(-1.0f, 0.0f, 0.0f), 90.0f, vec3(0.0f,0.0f,1.0f)));

	GLGeometry rightWall("plane.ob");
	rightWall.setWorldMatrix(rotate(translate(1.0f, 0.0f, 0.0f), -90.0f, vec3(0.0f,0.0f,1.0f)));

	GLuint ceilingTex = loadTexture("rocks.tga");
	GLuint wallsTex = loadTexture("rubble.tga");
	GLuint floorTex = loadTexture("Tiles01.tga");

	Player * player = new Player(0.1);

	double lastTime = glfwGetTime(), currentTime;
	do {

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		currentTime = glfwGetTime();

		updateFPSCounter(currentTime - lastTime);

		handleInput(currentTime - lastTime, player);
		updateWorld(currentTime, currentTime - lastTime, player);

		lastTime = currentTime;

		for(CGpass cgPass = cgGetFirstPass( cgTechnique ); cgPass; cgPass = cgGetNextPass( cgPass )) {
			cgSetPassState(cgPass);

			cgGLEnableTextureParameter( shaderTexture );

			cgGLSetupSampler( shaderTexture, floorTex);
			glBindTexture(GL_TEXTURE_2D,floorTex); //Binding the texture
			mWorld = floor.getWorldMatrix();
			updateMatrixParameters();
			floor.draw();

			cgGLSetupSampler( shaderTexture, ceilingTex);
			glBindTexture(GL_TEXTURE_2D,ceilingTex); //Binding the texture
			mWorld = ceiling.getWorldMatrix();
			updateMatrixParameters();
			ceiling.draw();

			cgGLSetupSampler( shaderTexture, wallsTex);
			glBindTexture(GL_TEXTURE_2D,wallsTex); //Binding the texture



			mWorld = leftWall.getWorldMatrix();
			updateMatrixParameters();
			leftWall.draw();

			mWorld = rightWall.getWorldMatrix();
			updateMatrixParameters();
			rightWall.draw();

			cgGLDisableTextureParameter( shaderTexture );
		}

		glfwSwapBuffers();

	} while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS && glfwGetWindowParam( GLFW_OPENED ) );

	floor.clean();
	ceiling.clean();
	leftWall.clean();
	rightWall.clean();
	Cleanup();
	exit(EXIT_SUCCESS);
}

