
LIBS= -L/usr/X11R6/lib -lm -lGL -lGLU -lCg -lCgGL  -lGLEW -lglfw
IDIR =/home/qwerty/workspace/glm-0.9.4.2/
PROJECT= Ex2
SOURCES= *.cpp *.cpp
OBJECTS= *.o
CC= g++
CXXFLAGS= -O3 -I$(IDIR) -Wall -ansi -pedantic -g

all: $(PROJECT) 
	
$(PROJECT): $(OBJECTS)
	@echo Linking
	$(CC) $(CXXFLAGS) $(OBJECTS) $(LIBS) -o $(PROJECT)
	@echo Compilation Complete

$(OBJECTS): $(SOURCES)
	@echo Compiling Sources
	$(CC) $(CXXFLAGS) -c $(SOURCES)

clean:
	@echo Deleting up $(OBJECTS) $(PROJECT)
	rm -f *.o;rm $(PROJECT)
