/*
 * Player.cpp
 *
 *  Created on: Mar 29, 2013
 *      Author: alex
 */

#include "Player.hpp"
#include "Flashlight.hpp"

Player::Player(){
	Player(0.5);
}

Player::Player(double speed){
	this->z = 0;
	this->speed = speed;
	this->flashlight = new Flashlight(0.5, glm::vec3(0,0,-1));
}

void Player::goForward(double walkingDuration){
	this->z += this->speed * walkingDuration;
}

void Player::goBackward(double walkingDuration){
	this->z -= this->speed * walkingDuration;
}

double Player::getPosition(){
	return this->z;
}

void Player::pointFlashlightAt(glm::vec3 direction){
	this->flashlight->pointTo(direction);
}

glm::vec3 Player::getFlashlightDirection(){
	return this->flashlight->getDirection();
}

Flashlight * Player::getFlashlight(){
	return this->flashlight;
}




