/*
 * Flashlight.hpp
 *
 *  Created on: Mar 30, 2013
 *      Author: alex
 */

#ifndef FLASHLIGHT_HPP_
#define FLASHLIGHT_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#define  MIN_ON_INTERVAL 2
#define MAX_ON_INTERVAL 5
#define MIN_OFF_INTERVAL  0.3
#define MAX_OFF_INTERVAL  2

class Flashlight{
	double intensity;
	glm::vec3 direction;
	glm::vec3 position;
	double intensityVaraition;
	bool on;
	double nextFlickerInterval;
	double nextIntensityVariationInterval;

public:
	Flashlight(double flashlightIntensity, glm::vec3 position);
	double getCurrentIntensity(double time);
	void pointTo(glm::vec3 direction);
	glm::vec3 getDirection();

private:
	void variateIntensity();
};


#endif
