/*
 * Player.hpp
 *
 *  Created on: Mar 29, 2013
 *      Author: alex
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "Flashlight.hpp"

// GLM headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

class Player{
	double z;
	double speed;

	Flashlight * flashlight;
public:
	Player();
	Player(double speed);
	void goForward(double walkingDuration);
	void goBackward(double walkingDuration);
	void pointFlashlightAt(glm::vec3 direction);
	double getPosition();
	glm::vec3 getFlashlightDirection();
	Flashlight * getFlashlight();
};



#endif /* PLAYER_HPP_ */
