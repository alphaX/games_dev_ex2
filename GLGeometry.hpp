/*
 * Geometry.h
 *
 *  Created on: Mar 30, 2013
 *      Author: alex
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include <GL/glew.h>
#include <GL/glfw.h>

// Cg headers
#include <Cg/cg.h>
#include <Cg/cgGL.h>

using namespace glm;

class GLGeometry {
	std::vector<vec3> vertices;
	std::vector<vec2> uvs;
	std::vector<vec3> normals;

	mat4 world;

	GLuint vertexbuffer, uvbuffer, normalbuffer;
public:
	GLGeometry(std::vector<vec3> & vertices, std::vector<vec2> & uvs, std::vector<vec3> & normals);
	GLGeometry(const char * objFile);
	void setWorldMatrix(mat4 world);
	mat4 getWorldMatrix();
	void draw();
	void clean();

private:
	void loadBuffers();
};

#endif /* GEOMETRY_H_ */
