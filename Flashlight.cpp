/*
 * Flashlight.cpp
 *
 *  Created on: Mar 30, 2013
 *      Author: alex
 */
#include "Flashlight.hpp"
#include  <cstdlib>

double rangeRandom(double min, double max){
	return (max-min)*(double)(rand()%1000)/(double)1000 + min;
}

Flashlight::Flashlight(double intensity, glm::vec3 position){
	this->intensity = intensity;
	this->position = position;
	this->direction = glm::vec3(0,0,-1);
	this->on = true;
	this->nextFlickerInterval += rangeRandom(MIN_ON_INTERVAL, MAX_ON_INTERVAL);
}

double Flashlight::getCurrentIntensity(double time){
	if(time > this->nextIntensityVariationInterval){
		this->variateIntensity();
		this->nextIntensityVariationInterval += (rand()%1000)/2000;
	}

	if( time > this->nextFlickerInterval){
		if(this->on){
			this->on = false;
			this->nextFlickerInterval += rangeRandom(MIN_OFF_INTERVAL, MAX_OFF_INTERVAL);
		} else{
			this->on = true;
			this->nextFlickerInterval += rangeRandom(MIN_ON_INTERVAL, MAX_ON_INTERVAL);
		}
	}

	if(this->on == false)
		return 0;

	return this->intensity + this->intensityVaraition;
}

void Flashlight::pointTo(glm::vec3 direction){
	this->direction = direction;
}

glm::vec3 Flashlight:: getDirection(){
	return this->direction;
}

void Flashlight::variateIntensity(){
	this->intensityVaraition = (double)(rand()%1000)/1000/6;
}


